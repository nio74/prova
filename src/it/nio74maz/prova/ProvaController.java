package it.nio74maz.prova;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class ProvaController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField txtMessage;

    @FXML
    private Button btnSend;

    @FXML
    void handleSend(ActionEvent event) {
    	this.txtMessage.setText("prova se funziona");

    }
}